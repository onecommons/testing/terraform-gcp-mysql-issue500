
resource "google_sql_database_instance" "default"{
    name= "mysql-instance"
    database_version = var.dbversion
    region = var.region
    settings {
         tier = "db-n1-standard-2"

    }

    ip_configuration {
        ip4_enabled ="True"
        private_network= "null"
        
    }
}

resource "google_sql_database" "db" {
    count = "1"
    name = var.dbname
    instance = "${google_sql_database_instance.dbinstance.name}"
    charset = "utf8"
  
}

resource "google_sql_user" "user" {
    name= var.dbuser
    instance = "${google_sql_database_instance.dbinstance.name}"
    host = "%"
    password = var.dbpass

  
}