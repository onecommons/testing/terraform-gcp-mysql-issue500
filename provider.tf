provider "google"{
    credentials = "onecommons-terraform.json"
    project=  var.project
    region=  "us-central1 "
    zone=   "us-central1-c"
}