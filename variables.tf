variable "project" {
    description = "This defines the project id "
    default = "onecommons-terraform"
  
}

variable "region" {
    description = "This defines the region where resources are configured"
    default = "us-central1"
  
}

variable "dbversion" {
    description = "This defines which database is being used"
    default= "MYSQL_8_0"
  
}

variable "dbname" {
    description = "This defines the name of the databse"
    default = "mysqldatabase"
  
}

variable "dbuser" {
  description = "This defines the database user name"
  default = "mysql-user"
}

variable "dbpass" {
    description = "This defines the database password"
    default= "password"
  
}