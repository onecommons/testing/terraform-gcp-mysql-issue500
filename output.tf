output "public_ip_address" {
  description = "The ip addresss assigned to the  instance"
  value       = google_sql_database_instance.default.public_ip_address
}